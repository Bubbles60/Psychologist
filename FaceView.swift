//
//  FaceView.swift
//  Happiness
//
//  Created by DFINLAY-AIR on 24/03/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.

import UIKit
// Step 1. Setup Protocol to provide data model (Smiliness)
protocol FaceviewDataSource: class {
    // The returnis an optional in case there is no value
    func smileinessForFaceView(sender: FaceView) -> Double?
}
@IBDesignable
class FaceView: UIView {
    // Setup constants
    private struct Scaling {
        static let FaceRadiusToEyeRadiusRatio: CGFloat = 10
        static let FaceRadiusToEyeOffsetRatio: CGFloat = 3
        static let FaceRadiusToEyeSeparationRatio: CGFloat = 1.5
        static let FaceRadiusToMouthWidthRatio: CGFloat = 1
        static let FaceRadiusToMouthHeightRatio: CGFloat = 3
        static let FaceRadiusToMouthOffsetRatio: CGFloat = 3
    }
    
    func scale (gesture: UIPinchGestureRecognizer){
        if gesture.state == .Changed{
            scale *= gesture.scale
            // REset to 1 so we only get increments
            gesture.scale = 1
        }
    }
    
    //
    @IBInspectable
    var scale: CGFloat = 0.9 {
        // Set property observer and redraw if scale changes
        didSet{setNeedsDisplay()}
    }

    // Vars with property observers
    @IBInspectable
    var lineWidth: CGFloat = 3 {
        // Set property observer and redraw if width changes
        didSet{setNeedsDisplay()}
    }
    @IBInspectable
    var color: UIColor = UIColor.blueColor(){
        // Set property observer and redraw if color changes
        didSet{setNeedsDisplay()}
    }
    // Step 2 Set up variable for where will data come from / who do i talk to
    weak var datasource: FaceviewDataSource?

    // Computed Value
    var faceCenter: CGPoint {
        // cant use center directly because it is the super view center
        // Convert parent center
        return convertPoint(center, fromView: superview)
    }
    
    var faceRadius :CGFloat {
        // Select whichever is smallest of width or height
        // divide by 2 its a radius
        return min(bounds.size.height,bounds.size.width)/2 * scale
    }
    
    
    override func drawRect(rect: CGRect) {
        // Draw face using bezier path
        let facePath = UIBezierPath(arcCenter: faceCenter, radius: faceRadius, startAngle: 0, endAngle: CGFloat(2*M_PI), clockwise: true)
        //
        facePath.lineWidth = lineWidth
        // will set fill and stroke
        color.set()
        facePath.stroke()
        // Draw eyes and smile
        bezierPathForEye(.Left).stroke()
        bezierPathForEye(.Right).stroke()
        // set smile
        // Step 3 use Datasource (Data source may be nil (optional chaining)
        let smiliness = datasource?.smileinessForFaceView(self) ?? 0.0
        let smilePath = bezierPathForSmile(smiliness)
        smilePath.stroke()
        
        
        
        
    }
    
    private enum Eye { case Left, Right }
    
    
    private func bezierPathForEye(whichEye: Eye) -> UIBezierPath
    {
        let eyeRadius = faceRadius / Scaling.FaceRadiusToEyeRadiusRatio
        let eyeVerticalOffset = faceRadius / Scaling.FaceRadiusToEyeOffsetRatio
        let eyeHorizontalSeparation = faceRadius / Scaling.FaceRadiusToEyeSeparationRatio
        
        var eyeCenter = faceCenter
        eyeCenter.y -= eyeVerticalOffset
        switch whichEye {
        case .Left: eyeCenter.x -= eyeHorizontalSeparation / 2
        case .Right: eyeCenter.x += eyeHorizontalSeparation / 2
        }
        
        let path = UIBezierPath(
            arcCenter: eyeCenter,
            radius: eyeRadius,
            startAngle: 0,
            endAngle: CGFloat(2*M_PI),
            clockwise: true
        )
        path.lineWidth = lineWidth
        return path
    }
    
    private func bezierPathForSmile(fractionOfMaxSmile: Double) -> UIBezierPath
    {
        // Note smile type is set by fractionOfMaxSmile 1 = Full smile -1 is frown
        
        let mouthWidth = faceRadius / Scaling.FaceRadiusToMouthWidthRatio
        let mouthHeight = faceRadius / Scaling.FaceRadiusToMouthHeightRatio
        let mouthVerticalOffset = faceRadius / Scaling.FaceRadiusToMouthOffsetRatio
        
        // Define height of curve / smile
        let smileHeight = CGFloat(max(min(fractionOfMaxSmile, 1), -1)) * mouthHeight
        
        let start = CGPoint(x: faceCenter.x - mouthWidth / 2, y: faceCenter.y + mouthVerticalOffset)
        let end = CGPoint(x: start.x + mouthWidth, y: start.y)
        let cp1 = CGPoint(x: start.x + mouthWidth / 3, y: start.y + smileHeight)
        let cp2 = CGPoint(x: end.x - mouthWidth / 3, y: cp1.y)
        
        let path = UIBezierPath()
        path.moveToPoint(start)
        // Draw curve for smile
        path.addCurveToPoint(end, controlPoint1: cp1, controlPoint2: cp2)
        path.lineWidth = lineWidth
        return path
    }

}
