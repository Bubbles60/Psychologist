//
//  ViewController.swift
//  Psychologist
//
//  Created by DFINLAY-AIR on 31/03/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.
//

import UIKit

class PsychologistViewController: UIViewController {

    @IBAction func nothing(sender: UIButton) {
        performSegueWithIdentifier("nothing", sender: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Fix title on detail view Nav controller is now destination
        var destination = segue.destinationViewController as? UIViewController
        if let navCon = destination as? UINavigationController {
            destination = navCon.visibleViewController
        }
        //
        if let hvc = destination as? HappinessViewController {
            
        //if let hvc = segue.destinationViewController as? HappinessViewController {
            if let identifier = segue.identifier {
                
                switch identifier {
                    case "sad": hvc.happiness = 0
                    case "happy": hvc.happiness = 100
                    case "nothing": hvc.happiness = 25
                    default: hvc.happiness = 50

                    
                }
            }
            
        }
    }


}

