//
//  HappinessViewController.swift
//  Happiness
//
//  Created by DFINLAY-AIR on 24/03/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.
//

import UIKit

// Step 4 Implement protocol
class HappinessViewController:
UIViewController, FaceviewDataSource {
    // Step 5 create pointer to view via outlet
    @IBOutlet weak var faceview: FaceView! {
        // Step 6 set data source / deegate for view
        didSet {
            // note "scale:" the : means we want parameters
            faceview.datasource = self
            faceview.addGestureRecognizer(UIPinchGestureRecognizer(target: faceview, action: "scale:"))
        }
    }
    
    private struct Constants {
        static let HappinessGestureScale: CGFloat = 4
    }
    
    @IBAction func changeHappiness(gesture: UIPanGestureRecognizer) {
        // Update model
        switch gesture.state {
        case .Ended: fallthrough
        case .Changed:
            let translation = gesture.translationInView(faceview)
            let happinessChange = -Int(translation.y / Constants.HappinessGestureScale)
            if happinessChange != 0 {
                happiness += happinessChange
                gesture.setTranslation(CGPointZero, inView: faceview)
            }
        default: break
        }
    }
    


    
    var happiness: Int = 10 { // 0 = very sad, 100 = ecstatic
        didSet {
            // Hapines must be a max of 100 min returns the minimum value of two numbers
            happiness = min(max(happiness, 0), 100)
            print("happiness = \(happiness)")
            updateUI()
        }
    }
    
    func updateUI() {
        // Redraw if model changes
        // Fix optional not set problem ? called from model in prepare
        // ? Means ignore statement if nil (optional chaining)
        faceview?.setNeedsDisplay()
        // Add Title in Face View
        title = "\(happiness)"
    }
    
    func smileinessForFaceView(sender: FaceView) -> Double? {
        // Interprate model for view convert 50 to smiliness range
        return Double(happiness-50)/(50)
    }
}


